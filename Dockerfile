# build stage
FROM node:slim as builder
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build  

# run stage
FROM node:alpine
WORKDIR /app
COPY --from=builder /app/dist/index.js /app/dist/
COPY --from=builder /app/package.json /app/
RUN npm install --production
EXPOSE 3000
CMD npm start
